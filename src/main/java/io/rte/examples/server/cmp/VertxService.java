package io.rte.examples.server.cmp;

import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpServer;
import io.vertx.ext.web.Route;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.RedirectAuthHandler;
import io.vertx.ext.web.handler.StaticHandler;
import io.vertx.ext.web.handler.sockjs.SockJSHandler;
import io.vertx.ext.web.handler.sockjs.SockJSHandlerOptions;
import io.vertx.ext.web.handler.sockjs.SockJSSocket;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

/**
 * Author:      moritz thielcke
 * Date:        04.01.2021
 * Description:
 */
@Service
public class VertxService {
    private final Vertx vertx;
    private final Router router;
    private HttpServer httpServer;
    private final SockJSHandler sockJSHandler;

    public VertxService(){
        vertx = Vertx.vertx();
        sockJSHandler = SockJSHandler.create(vertx, new SockJSHandlerOptions().setHeartbeatInterval(2000));
        httpServer = vertx.createHttpServer();
        router = Router.router(vertx);
        httpServer.requestHandler(router::handle);
        router.route("/static/*").handler(StaticHandler.create());
        router.route("/").handler( e -> {
            e.redirect("/static/");
        });
    }

    /***
     * mounts a sub router and registers a sockjs "socket" to it
     * @param path the uri-path for the subrouter
     * @param handler your sockjs handler
     * @return the created Route
     */
    public Route mountSockJS(String path, Handler<SockJSSocket> handler){
        return router.mountSubRouter("/socket", sockJSHandler.socketHandler(handler));
    }

    @Bean
    public HttpServer httpServer(){
        return httpServer;
    }

    @Bean
    public Vertx vertx(){
        return vertx;
    }

    @Bean
    public Router router(){
        return router;
    }

}
