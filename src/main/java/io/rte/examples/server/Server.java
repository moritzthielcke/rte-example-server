package io.rte.examples.server;

import io.rte.examples.server.cmp.VertxService;
import io.vertx.core.http.HttpServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Author:      moritz thielcke
 * Date:        04.01.2021
 * Description:
 */
@SpringBootApplication
public class Server implements CommandLineRunner {
    private static final Logger log = LoggerFactory.getLogger(Server.class);
    private final VertxService vertxSrv;
    private final HttpServer server;

    public static void main(String[] args) {
        SpringApplication.run(Server.class);
    }


    public Server(@Autowired VertxService vertxSrv, @Autowired HttpServer server) {
        this.vertxSrv = vertxSrv;
        this.server = server;
    }

    @Override
    public void run(String... args) {
        log.info("Starting server");
        vertxSrv.mountSockJS("/socket", connection -> {
            log.info("hello socket {} from {} ", connection.writeHandlerID(), connection.remoteAddress());
            connection.handler(connection::write);
        });
        server.listen(8080);
    }
}
